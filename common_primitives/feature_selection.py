"""
References:
"https://github.com/jundongl/scikit-feature"
"http://featureselection.asu.edu/html/skfeature.html"
"""
import os
import typing
from typing import Dict, Union, List

import d3m.metadata.base as metadata_module
from d3m import container, utils as d3m_utils
from d3m.metadata import hyperparams, params
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

import common_primitives

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    selection_method = hyperparams.Enumeration(
        values=['SPEC', 'fisher_score', 'reliefF', 'CIFE', 'f_score', 'chi_square'],
        default='SPEC',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        description="different method to choose for feature selection")

    percentage_selected_features = hyperparams.Uniform(
        default=0.5,
        upper=1,
        lower=0,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        description="percentage of features to select, between 0 and 1")


class Params(params.Params):
    selected_indices: typing.List[int]


class FeatureSelectionPrimitive(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A collection of Feature selection methods wrapped from SKfeatures.
    """

    metadata = metadata_module.PrimitiveMetadata({
        'id': 'f32dcb25-4cd0-4bb9-9408-ade1edfa2b53',
        "version": "0.1.0",
        "name": "Feature Selection",
        "source": {
            'name': common_primitives.__author__,
            'contact': 'mailto:dmartinez05@tamu.edu',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/feature_selection.py',
                'https://gitlab.com/datadrivendiscovery/common-primitives.git',
            ],
        },
        "installation": [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        "python_path": "d3m.primitives.feature_selection.skfeature.Common",
        "algorithm_types": [metadata_module.PrimitiveAlgorithmType.ENCODE_ONE_HOT],
        "primitive_family": "FEATURE_SELECTION",
    })

    _training_inputs: typing.Optional[Inputs]
    _training_outputs: typing.Optional[Outputs]
    _idx: List[int]
    _selection_method: str
    _percentage_selected_features: float
    _n_selected_features: typing.Optional[int]
    _remove_columns_idx: List[int]

    def __init__(self, *, hyperparams: Hyperparams, random_seed: int = 0, docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        self._training_inputs = None
        self._training_outputs = None
        self._idx = []
        self._selection_method = hyperparams['selection_method']
        self._percentage_selected_features = hyperparams['percentage_selected_features']
        self._n_selected_features = None
        self._remove_columns_idx = []

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        assert self._training_inputs is not None
        assert self._training_outputs is not None

        self._n_selected_features = int(self._training_inputs.values.shape[1] * self._percentage_selected_features)

        if self._selection_method == 'SPEC':
            from skfeature.function.similarity_based import SPEC
            score = SPEC.spec(self._training_inputs.values)
            idx = SPEC.feature_ranking(score)

        elif self._selection_method == 'fisher_score':
            from skfeature.function.similarity_based import fisher_score
            score = fisher_score.fisher_score(self._training_inputs.values, self._training_outputs.values[:, 0])
            idx = fisher_score.feature_ranking(score)

        elif self._selection_method == 'reliefF':
            from skfeature.function.similarity_based import reliefF
            score = reliefF.reliefF(self._training_inputs.values, self._training_outputs.values[:, 0])
            idx = reliefF.feature_ranking(score)

        elif self._selection_method == 'CIFE':
            from skfeature.function.information_theoretical_based import CIFE
            idx = CIFE.cife(self._training_inputs.values, self._training_outputs.values[:, 0])[0]

        elif self._selection_method == 'f_score':
            from skfeature.function.statistical_based import f_score
            score = f_score.f_score(self._training_inputs.values, self._training_outputs.values[:, 0])
            idx = f_score.feature_ranking(score)

        elif self._selection_method == 'chi_square':
            from skfeature.function.statistical_based import chi_square
            score = chi_square.chi_square(self._training_inputs.values, self._training_outputs.values[:, 0])
            idx = chi_square.feature_ranking(score)

        else:
            raise ValueError("Such feature selection method is NOT found.")

        self._idx = idx[0:self._n_selected_features].tolist()

        self._remove_columns_idx = idx[self._n_selected_features:]
        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        outputs = inputs.iloc[:, self._idx]
        outputs.metadata = outputs.metadata.remove_columns(self._remove_columns_idx)
        return CallResult(outputs)

    def set_params(self, *, params: Params) -> None:
        self._idx = params['selected_indices']

    def get_params(self) -> Params:
        return Params(
            selected_indices=self._idx
        )
