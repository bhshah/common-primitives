import os
import pickle
import unittest

from d3m import container, utils
from d3m.metadata import base as metadata_base

from common_primitives import dataset_to_dataframe, extract_columns_semantic_types, column_parser, random_forest, compute_unique_values
from common_primitives import construct_confidence


class ConstructConfidenceTest(unittest.TestCase):
    def _get_iris(self):
        dataset_doc_path = os.path.abspath(
            os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        hyperparams_class = \
            dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())

        dataframe = primitive.produce(inputs=dataset).value

        return dataframe

    def _get_iris_columns(self):
        dataframe = self._get_iris()

        # We set custom metadata on columns.
        for column_index in range(1, 5):
            dataframe.metadata = dataframe.metadata.update_column(column_index, {'custom_metadata': 'attributes'})
        for column_index in range(5, 6):
            dataframe.metadata = dataframe.metadata.update_column(column_index, {'custom_metadata': 'targets'})

        # We set semantic types like runtime would.
        dataframe.metadata = dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                  'https://metadata.datadrivendiscovery.org/types/Target')
        dataframe.metadata = dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                  'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        dataframe.metadata = dataframe.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                  'https://metadata.datadrivendiscovery.org/types/Attribute')

        # Parsing.
        hyperparams_class = \
            column_parser.ColumnParserPrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults())
        dataframe = primitive.produce(inputs=dataframe).value

        hyperparams_class = \
            extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code'][
                'class_type_arguments']['Hyperparams']

        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(
            hyperparams=hyperparams_class.defaults().replace(
                {'semantic_types': ('https://metadata.datadrivendiscovery.org/types/Attribute',)}))
        attributes = primitive.produce(inputs=dataframe).value

        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(
            hyperparams=hyperparams_class.defaults().replace(
                {'semantic_types': ('https://metadata.datadrivendiscovery.org/types/SuggestedTarget',)}))
        targets = primitive.produce(inputs=dataframe).value

        return dataframe, attributes, targets

    def test_confidence(self):
        dataframe, attributes, targets = self._get_iris_columns()

        learner_class = random_forest.RandomForestClassifierPrimitive
        learner_class_hp = learner_class.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        learner_instance = learner_class( hyperparams=learner_class_hp.defaults())
        learner_instance.set_training_data(inputs=attributes, outputs=targets)
        learner_instance.fit()

        update_metadata_class = compute_unique_values.ComputeUniqueValuesPrimitive
        update_metadata_class_hp = update_metadata_class.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        update_metadata_instance = update_metadata_class(hyperparams=update_metadata_class_hp.defaults())
        update_metadata_instance.set_training_data(inputs=dataframe)
        update_metadata_instance.fit()
        distinct_values = update_metadata_instance.produce(inputs=dataframe).value

        confidence_class = construct_confidence.ConstructConfidencePrimitive
        confidence_prim_hp = confidence_class.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        confidence_instance = construct_confidence.ConstructConfidencePrimitive(hyperparams=confidence_prim_hp.defaults().replace({'primitive_learner': learner_instance}))
        result = confidence_instance.produce(inputs=distinct_values, reference=dataframe).value

        # Check that the result dataframe size is 3 times longer, since we only have 3 labels.
        self.assertEqual(len(result), 3 * len(dataframe))

        # Check that we have the required columns d3mIndex, labels, confidence.
        self.assertEqual(list(result.columns), ['d3mIndex', 'species', 'confidence'])

        # Check that we have 3 times each index.
        index_column = list(dataframe['d3mIndex'].loc[dataframe.index.repeat(3)].reset_index(drop=True).values)
        self.assertEqual(list(result['d3mIndex'].values), index_column)

        # Check that the labels appear for every index.
        labels = tuple(sorted(set(['Iris-setosa', 'Iris-virginica', 'Iris-versicolor'])))
        input_labels = container.DataFrame(labels)
        input_labels = input_labels.append([input_labels] * (len(dataframe) - 1)).reset_index(drop=True)
        input_labels = list(input_labels.transpose().values[0])
        self.assertEqual(list(result['species'].values), input_labels)

        # Check metadata.
        self.assertIn('https://metadata.datadrivendiscovery.org/types/PrimaryKey', result.metadata.query((metadata_base.ALL_ELEMENTS, 0,))['semantic_types'])
        self.assertIn('https://metadata.datadrivendiscovery.org/types/Target', result.metadata.query((metadata_base.ALL_ELEMENTS, 1,))['semantic_types'])
        self.assertIn('https://metadata.datadrivendiscovery.org/types/LogLikelihood', result.metadata.query((metadata_base.ALL_ELEMENTS, 2,))['semantic_types'])


if __name__ == '__main__':
    unittest.main()
