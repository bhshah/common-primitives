import os
import unittest

from common_primitives import dataset_to_dataframe, extract_columns_semantic_types, column_parser
from d3m import container
from d3m.metadata import base as metadata_base

from common_primitives import dataframe_sampling


class DataframeSamplingPrimitiveTestCase(unittest.TestCase):

    def _get_iris(self):
        dataset_doc_path = os.path.abspath(
            os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        hyperparams_class = \
            dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())

        dataframe = primitive.produce(inputs=dataset).value

        return dataframe

    def _get_iris_columns(self):
        dataframe = self._get_iris()

        # We set custom metadata on columns.
        for column_index in range(1, 5):
            dataframe.metadata = dataframe.metadata.update_column(column_index, {'custom_metadata': 'attributes'})
        for column_index in range(5, 6):
            dataframe.metadata = dataframe.metadata.update_column(column_index, {'custom_metadata': 'targets'})

        # We set semantic types like runtime would.
        dataframe.metadata = dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                  'https://metadata.datadrivendiscovery.org/types/Target')
        dataframe.metadata = dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                  'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        dataframe.metadata = dataframe.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                     'https://metadata.datadrivendiscovery.org/types/Attribute')

        # Parsing.
        hyperparams_class = \
            column_parser.ColumnParserPrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults())
        dataframe = primitive.produce(inputs=dataframe).value

        hyperparams_class = \
            extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code'][
                'class_type_arguments']['Hyperparams']

        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(
            hyperparams=hyperparams_class.defaults().replace(
                {'semantic_types': ('https://metadata.datadrivendiscovery.org/types/Attribute',)}))
        attributes = primitive.produce(inputs=dataframe).value

        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(
            hyperparams=hyperparams_class.defaults().replace(
                {'semantic_types': ('https://metadata.datadrivendiscovery.org/types/SuggestedTarget',)}))
        targets = primitive.produce(inputs=dataframe).value

        return dataframe, attributes, targets

    def test_basic(self):
        dataframe, attributes, targets = self._get_iris_columns()
        self.assertEqual(list(targets.columns), ['species'])

        n_output_dataframes = [1, 2, 3]

        hyperparams = dataframe_sampling.DataFrameSamplingPrimitive.metadata.get_hyperparams().defaults()
        for n in n_output_dataframes:
            hp = hyperparams.replace({'n_output_dataframes': n})
            primitive = dataframe_sampling.DataFrameSamplingPrimitive(hyperparams=hp)
            call_result = primitive.produce(inputs=dataframe)

            # check we have the correct number of outputs
            self.assertEqual(len(call_result.value), n)
            for i in range(n):
                df_metadata = call_result.value[i].metadata
                # check general metadata stays tha same.
                self.assertEqual(df_metadata.query((metadata_base.ALL_ELEMENTS,)),
                                 dataframe.metadata.query((metadata_base.ALL_ELEMENTS,)))
                # input size has 150 rows and default hp is 80% so samples should be 120
                self.assertEqual(df_metadata.query(())['dimension']['length'], 120)

                # check if the samples
                self.assertEqual(len(call_result.value[i]), 120)

    def test_distributions(self):
        dataframe, attributes, targets = self._get_iris_columns()
        self.assertEqual(list(targets.columns), ['species'])

        hyperparams = dataframe_sampling.DataFrameSamplingPrimitive.metadata.get_hyperparams().defaults()
        for distribution in hyperparams.configuration['distribution'].values:
            hp = hyperparams.replace({'distribution': distribution})

            primitive = dataframe_sampling.DataFrameSamplingPrimitive(hyperparams=hp)
            call_result = primitive.produce(inputs=dataframe)

            df_metadata = call_result.value[0].metadata
            # check general metadata stays tha same.
            self.assertEqual(df_metadata.query((metadata_base.ALL_ELEMENTS,)),
                             dataframe.metadata.query((metadata_base.ALL_ELEMENTS,)))
            # input size has 150 rows and default hp is 80% so samples should be 120
            self.assertEqual(df_metadata.query(())['dimension']['length'], 120)

            # check if the samples
            self.assertEqual(len(call_result.value[0]), 120)


if __name__ == '__main__':
    unittest.main()
