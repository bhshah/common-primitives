import os
import unittest

from d3m import container
from d3m.metadata import base as metadata_base

from common_primitives import dataset_to_dataframe, compute_unique_values


class ComputeUniqueTargetValuePrimitiveTestCase(unittest.TestCase):
    # TODO: Make this part of metadata API.
    #       Something like setting a semantic type for given columns.
    def _mark_all_targets(self, dataset, targets):
        for target in targets:
            dataset.metadata = dataset.metadata.add_semantic_type((target['resource_id'], metadata_base.ALL_ELEMENTS, target['column_index']), 'https://metadata.datadrivendiscovery.org/types/Target')
            dataset.metadata = dataset.metadata.add_semantic_type((target['resource_id'], metadata_base.ALL_ELEMENTS, target['column_index']), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
            dataset.metadata = dataset.metadata.remove_semantic_type((target['resource_id'], metadata_base.ALL_ELEMENTS, target['column_index']), 'https://metadata.datadrivendiscovery.org/types/Attribute')

    def _get_iris_dataframe(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self._mark_all_targets(dataset, [{'resource_id': 'learningData', 'column_index': 5}])

        hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()

        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = primitive.produce(inputs=dataset)

        dataframe = call_metadata.value

        return dataframe

    def test_correct_order_target(self):
        dataframe = self._get_iris_dataframe()

        hyperparams_class = compute_unique_values.ComputeUniqueValuesPrimitive.metadata.get_hyperparams()
        primitive_instance = compute_unique_values.ComputeUniqueValuesPrimitive(hyperparams=hyperparams_class.defaults())

        primitive_instance.set_training_data(inputs=dataframe)
        primitive_instance.fit()
        results = primitive_instance.produce(inputs=dataframe)

        target_metadata = results.value.metadata.query((metadata_base.ALL_ELEMENTS, 5))

        self.assertIn('all_distinct_values', target_metadata)
        self.assertEqual(target_metadata['all_distinct_values'],  ('Iris-setosa', 'Iris-versicolor', 'Iris-virginica'))

    def test_multiple_columns(self):
        dataframe = self._get_iris_dataframe()

        hyperparams_class = compute_unique_values.ComputeUniqueValuesPrimitive.metadata.get_hyperparams()
        primitive_instance = compute_unique_values.ComputeUniqueValuesPrimitive(
            hyperparams=hyperparams_class.defaults().replace({'columns': (0, 1, 2)}))

        primitive_instance.set_training_data(inputs=dataframe)
        primitive_instance.fit()
        results = primitive_instance.produce(inputs=dataframe)

        for i in range(3):
            unique_values = tuple(sorted(set(dataframe.iloc[:, i].unique().tolist())))
            column_metadata = results.value.metadata.query((metadata_base.ALL_ELEMENTS, i))
            self.assertIn('all_distinct_values', column_metadata)
            self.assertEqual(column_metadata['all_distinct_values'], unique_values)


if __name__ == '__main__':
    unittest.main()
