import unittest
import pickle

from d3m.metadata import base as metadata_base
from d3m import container
from d3m.primitive_interfaces.base import PrimitiveBase
from d3m.exceptions import PrimitiveNotFittedError
from d3m.container.pandas import DataFrame
from pandas.util.testing import assert_frame_equal

import os
from common_primitives import dataset_to_dataframe, denormalize, fastai_tl

class TestFastAIWrapper(unittest.TestCase):
    parsed_dataframe = None
    train_set=None
    @classmethod
    def setUpClass(cls):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'image_dataset_2', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))
        hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())
        call_metadata = primitive.produce(inputs=dataset)
        dataframe = call_metadata.value
        denormalize_hyperparams_class = denormalize.DenormalizePrimitive.metadata.get_hyperparams()
        denromalize_primitive = denormalize.DenormalizePrimitive(hyperparams=denormalize_hyperparams_class.defaults())
        dataset = denromalize_primitive.produce(inputs=dataset).value
        dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()
        dataframe_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataframe_hyperparams_class.defaults())
        cls.parsed_dataframe= dataframe_primitive.produce(inputs=dataset).value
        cls.parsed_dataframe.metadata = cls.parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/Target')
        cls.parsed_dataframe.metadata = cls.parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        cls.parsed_dataframe.metadata = cls.parsed_dataframe.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/Attribute')
        cls.parsed_dataframe.metadata = cls.parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/FileName')
        cls.train_set = targets = cls.parsed_dataframe
        
    def create_learner(self, hyperparams):
        clf = fastai_tl.FastAIWrapperPrimitive(hyperparams=hyperparams)
        return clf
    def set_training_data_on_learner(self, learner, **args):
        learner.set_training_data(**args)
    def fit_learner(self, learner: PrimitiveBase):
        learner.fit()
    def produce_learner(self, learner, **args):
        return learner.produce(**args)
    def basic_fit(self, hyperparams):
        learner = self.create_learner(hyperparams)
        training_data_args = self.set_data(hyperparams)
        self.set_training_data_on_learner(learner, **training_data_args)
        self.assertRaises(PrimitiveNotFittedError, learner.produce, inputs=training_data_args.get("inputs"))
        self.fit_learner(learner)
        assert len(learner._training_indices) > 0
        output = self.produce_learner(learner, inputs=training_data_args.get("inputs"))
        return output, learner, training_data_args
    def pickle(self, hyperparams):
        output, learner, training_data_args = self.basic_fit(hyperparams)
        params = learner.get_params()
        learner.set_params(params=params)
        model = pickle.dumps(learner)
        new_clf = pickle.loads(model)
        new_output = new_clf.produce(inputs=training_data_args.get("inputs"))
        assert_frame_equal(new_output.value, output.value)
    def set_data(self, hyperparams):
        hyperparams = hyperparams.get("use_semantic_types")
        if hyperparams:
            #with semantic types
            return {"inputs": self.parsed_dataframe, "outputs": self.parsed_dataframe}
        else:
            #without semantic types
            return {"inputs": self.parsed_dataframe.select_columns([1]), "outputs": self.parsed_dataframe.select_columns([2])}
    def get_transformed_indices(self, learner):
        return learner._target_column_indices
    def new_return_checker(self, output, indices):
        semantic_types_to_remove = set(['https://metadata.datadrivendiscovery.org/types/TrueTarget', 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'])
        semantic_types_to_add = set(['https://metadata.datadrivendiscovery.org/types/PredictedTarget'])
        input_target = self.train_set.select_columns(list(indices))
        for i in range(len(output.columns)):
            input_semantic_types = input_target.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types")
            output_semantic_type = set(
                output.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            transformed_input_semantic_types = set(input_semantic_types) - semantic_types_to_remove
            transformed_input_semantic_types = transformed_input_semantic_types.union(semantic_types_to_add)
            assert output_semantic_type == transformed_input_semantic_types
    def append_return_checker(self, output, indices):
        for i in range(len(self.train_set.columns)):
            input_semantic_types = set(self.train_set.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            assert output_semantic_type == input_semantic_types
        self.new_return_checker(
            output.value.select_columns(list(range(len(self.train_set.columns), len(output.value.columns)))),
            indices)
    def replace_return_checker(self, output, indices):
        for i in range(len(self.train_set.columns)):
            if i in indices:
                continue
            input_semantic_types = set(self.train_set.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            assert output_semantic_type == input_semantic_types
        self.new_return_checker(output.value.select_columns(list(indices)), indices)
    def test_with_semantic_types(self):
        hyperparams = fastai_tl.Hyperparams.defaults().replace({"use_semantic_types": True})
        self.pickle(hyperparams)
    def test_without_semantic_types(self):
        hyperparams = fastai_tl.Hyperparams.defaults()
        self.pickle(hyperparams)
    def test_with_new_return_result(self):
        hyperparams = fastai_tl.Hyperparams.defaults().replace({"return_result": 'new', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.new_return_checker(output.value, indices)
    def test_with_append_return_result(self):
    # check for adding filename 
        hyperparams = fastai_tl.Hyperparams.defaults().replace({"return_result": 'append', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.append_return_checker(output, indices)
    def test_with_replace_return_result(self):
        hyperparams = fastai_tl.Hyperparams.defaults().replace({"return_result": 'replace', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.replace_return_checker(output, indices)

if __name__ == '__main__':
    # We want to test the running of the code without errors and not the correctness of it
    unittest.main()