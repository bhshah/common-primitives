import json
from d3m import index
from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from d3m.index import get_primitive

# -> dataset_to_dataframe -> column_parser -> extract_columns_by_semantic_types(attributes) -> imputer -> random_forest
#                                             extract_columns_by_semantic_types(targets)    ->            ^

# Creating pipeline
pipeline_description = Pipeline()
pipeline_description.add_input(name='inputs')

# Step 0: dataset_to_dataframe
step_0 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.dataset_to_dataframe.Common'))
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 1: profiler
step_1 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.schema_discovery.profiler.Common'))
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_hyperparameter(name='categorical_max_ratio_distinct_values', argument_type=ArgumentType.VALUE, data=1)
step_1.add_hyperparameter(name='categorical_max_absolute_distinct_values', argument_type=ArgumentType.VALUE, data=None)
step_1.add_output('produce')
pipeline_description.add_step(step_1)

# Step 2: column_parser
step_2 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.column_parser.Common'))
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_output('produce')
pipeline_description.add_step(step_2)

# Step 3: extract_columns_by_semantic_types(attributes)
step_3 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.extract_columns_by_semantic_types.Common'))
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_3.add_output('produce')
step_3.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                          data=['https://metadata.datadrivendiscovery.org/types/Attribute'])
pipeline_description.add_step(step_3)

# Step 4: extract_columns_by_semantic_types(targets)
step_4 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.extract_columns_by_semantic_types.Common'))
step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_4.add_output('produce')
step_4.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                          data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
pipeline_description.add_step(step_4)

attributes = 'steps.3.produce'
targets = 'steps.4.produce'

# Step 5: imputer
step_5 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_cleaning.imputer.SKlearn'))
step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)
step_5.add_output('produce')
pipeline_description.add_step(step_5)
attributes = 'steps.5.produce'

# Step 6: feature_selection
step_6 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.feature_selection.skfeature.Common'))
step_6.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)
step_6.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
step_6.add_output('produce')
pipeline_description.add_step(step_6)
attributes = 'steps.6.produce'

# Step 7: extra_trees
step_7 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.extra_trees.SKlearn'))
step_7.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)
step_7.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
step_7.add_output('produce')
pipeline_description.add_step(step_7)

step_8 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.construct_predictions.Common'))
step_8.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.7.produce')
step_8.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_8.add_output('produce')
pipeline_description.add_step(step_8)

# Final Output
pipeline_description.add_output(name='output predictions', data_reference='steps.8.produce')


# Output to json
print(pipeline_description.to_json(indent=4))
with open('{}.json'.format(pipeline_description.id), 'w') as file:
    pipeline_description.to_json(file, indent=2, sort_keys=True, ensure_ascii=False)

