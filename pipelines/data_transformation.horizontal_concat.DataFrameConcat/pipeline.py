from d3m import index
from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep

# Creating pipeline
pipeline_description = Pipeline()
pipeline_description.add_input(name='inputs')

# Step 0: dataset_to_dataframe
step_0 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.dataset_to_dataframe.Common'))
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 1: profiler
step_1 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.schema_discovery.profiler.Common'))
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_hyperparameter(name='categorical_max_ratio_distinct_values', argument_type=ArgumentType.VALUE, data=1)
step_1.add_hyperparameter(name='categorical_max_absolute_distinct_values', argument_type=ArgumentType.VALUE, data=None)
step_1.add_output('produce')
pipeline_description.add_step(step_1)

# Step 2: column_parser
step_2 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.column_parser.Common'))
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_output('produce')
pipeline_description.add_step(step_2)

# Step 3: imputer
step_3 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_cleaning.imputer.SKlearn'))
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_3.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
step_3.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
step_3.add_output('produce')
pipeline_description.add_step(step_3)
dataframe = 'steps.3.produce'

# Step 6: random_forest
step_4 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.random_forest.SKlearn'))
step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_4.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_4.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
step_4.add_output('produce')
pipeline_description.add_step(step_4)

# Step 5: ada_boost
step_5 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.ada_boost.SKlearn'))
step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_5.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_5.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
step_5.add_output('produce')
pipeline_description.add_step(step_5)

# Step 6: gradient_boosting
step_6 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.gradient_boosting.SKlearn'))
step_6.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_6.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_6.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
step_6.add_output('produce')
pipeline_description.add_step(step_6)

# Step 7: Multi_concat
step_7 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.multi_horizontal_concat.Common'))
step_7.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=['steps.4.produce', 'steps.5.produce',
                                                                                          'steps.6.produce'])
step_7.add_output('produce')
pipeline_description.add_step(step_7)

# Step 8: replace_semantic_types because things get broken when passing multiple targets around
step_8 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.replace_semantic_types.Common'))
step_8.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.7.produce')
step_8.add_hyperparameter(name='from_semantic_types', argument_type=ArgumentType.VALUE,
                           data=['https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                                 'https://metadata.datadrivendiscovery.org/types/Target']
                           )
step_8.add_hyperparameter(name='to_semantic_types', argument_type=ArgumentType.VALUE,
                           data=['https://metadata.datadrivendiscovery.org/types/Attribute']
                           )
step_8.add_output('produce')
pipeline_description.add_step(step_8)

# Step 9: ordinal_encoder
step_9 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.ordinal_encoder.SKlearn'))
step_9.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.8.produce')
step_9.add_output('produce')
pipeline_description.add_step(step_9)

# Step 10: horizontal_concat
step_10 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.horizontal_concat.DataFrameCommon'))
step_10.add_argument(name='left', argument_type=ArgumentType.CONTAINER, data_reference='steps.3.produce')
step_10.add_argument(name='right', argument_type=ArgumentType.CONTAINER, data_reference='steps.9.produce')
step_10.add_output('produce')
pipeline_description.add_step(step_10)

# Step 11: rename_duplicate_name because things break easily when we have repeated names in the columns
step_11 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.rename_duplicate_name.DataFrameCommon'))
step_11.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.10.produce')
step_11.add_output('produce')
pipeline_description.add_step(step_11)

# Step 12: random_forest
step_12 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.random_forest.SKlearn'))
step_12.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.11.produce')
step_12.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.11.produce')
step_12.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
step_12.add_output('produce')
pipeline_description.add_step(step_12)

step_13 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.construct_predictions.Common'))
step_13.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.12.produce')
step_13.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_13.add_output('produce')
pipeline_description.add_step(step_13)

# Final Output
pipeline_description.add_output(name='output predictions', data_reference='steps.13.produce')


# Output to json
print(pipeline_description.to_json(indent=4))
with open('{}.json'.format(pipeline_description.id), 'w') as file:
    pipeline_description.to_json(file, indent=2, sort_keys=True, ensure_ascii=False)
print(pipeline_description.id)

