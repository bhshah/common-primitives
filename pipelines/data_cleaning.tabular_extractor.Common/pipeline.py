from d3m import index
from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep

# Creating pipeline
pipeline_description = Pipeline()
pipeline_description.add_input(name='inputs')

# Step 0: dataset_to_dataframe
step_0 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.dataset_to_dataframe.Common'))
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 1: profiler
step_1 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.schema_discovery.profiler.Common'))
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_hyperparameter(name='categorical_max_ratio_distinct_values', argument_type=ArgumentType.VALUE, data=0.2)
step_1.add_hyperparameter(name='categorical_max_absolute_distinct_values', argument_type=ArgumentType.VALUE, data=None)
step_1.add_output('produce')
pipeline_description.add_step(step_1)

# Step 2: column_parser
step_2 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.column_parser.Common'))
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_output('produce')
pipeline_description.add_step(step_2)

# Step 3: remove_columns
step_3 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.remove_columns.Common'))
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_3.add_hyperparameter(name='columns', argument_type=ArgumentType.VALUE, data=[1, 2, 8, 17])
step_3.add_output('produce')
pipeline_description.add_step(step_3)

# Step 4: imputer
step_4 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_cleaning.imputer.SKlearn'))
step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.3.produce')
step_4.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
step_4.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
step_4.add_output('produce')
pipeline_description.add_step(step_4)

# Step 5: tabular_extractor
step_5 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_cleaning.tabular_extractor.Common'))
step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.4.produce')
step_5.add_output('produce')
pipeline_description.add_step(step_5)
dataframe = 'steps.5.produce'

# Step 6: random_forest
step_6 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.random_forest.SKlearn'))
step_6.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_6.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=dataframe)
step_6.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
step_6.add_output('produce')
pipeline_description.add_step(step_6)

step_7 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.construct_predictions.Common'))
step_7.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.6.produce')
step_7.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_7.add_output('produce')
pipeline_description.add_step(step_7)

# Final Output
pipeline_description.add_output(name='output predictions', data_reference='steps.7.produce')
# pipeline_description.add_output(name='output predictions', data_reference='steps.0.produce')

# Output to json
print(pipeline_description.to_json(indent=4))
with open('{}.json'.format(pipeline_description.id), 'w') as file:
    pipeline_description.to_json(file, indent=2, sort_keys=True, ensure_ascii=False)
print(pipeline_description.id)
